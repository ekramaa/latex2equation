## Latex to equation

This application showcases how simple LaTeX equation can be parsed using LARK and evaluated using NumPy.

Code is written in Python 3.7, venv is created using Poetry. To launch the script, follow steps bellow:

``` sh
# create venv
poetry install

# activate venv
poetry shell

# run calculation
python parse.py
```
