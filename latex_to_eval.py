import logging
from dynaconf import settings

from lark import Transformer

import numpy


class LatexToEval(Transformer):
    """
    Extends Lark Tranformer to convert LaTeX expression into Python code for evaluation.
    """
    def __init__(self):
        # initialize logging format and level
        logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                            datefmt='%Y-%m-%d:%H:%M:%S',
                            level=logging.INFO)

    def division(self, items):
        """
        Transform/evaluate division node.
        """
        division = f"numpy.divide({items[0]}, {items[1]})"
        logging.info(division) 
        return numpy.divide(items[0], items[1])

    def multiplication(self, items):
        """
        Transform/evaluate multiplication node.
        """
        multiplication = f"numpy.multiply({items[0]}, {items[1]})"
        logging.info(multiplication) 
        return numpy.multiply(items[0], items[1])
    
    def addition(self, items):
        """
        Transform/evaluate addition node.
        """
        addition = f"numpy.add({items[0]}, {items[1]})"
        logging.info(addition) 
        return numpy.add(items[0], items[1])

    def subtraction(self, items):
        """
        Transform/evaluate subtraction node.
        """
        subtraction = f"numpy.subtract({items[0]}, {items[1]})"
        logging.info(subtraction) 
        return numpy.subtract(items[0], items[1])
    
    def name(self, items):
        """
        Transform token_string node by retrieving value from MOCK settings.
        """
        return settings.MOCK[items[0]]

    def number(self, n):
        """
        Transform number to float.
        """
        (n,) = n
        return float(n)
