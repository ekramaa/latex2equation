from dynaconf import settings

from lark import Lark

from latex_to_eval import LatexToEval


# equation sample
equation: str = """
\\begin{cases}
\\frac{{3}\+{6}\\frac{25}{4}}{\\frac{\\vstupJedna}{\\vstupDva}}
\\frac{\\vstupTri}{{\\vstupCtyri}-{2}\\times {3}}
\end{cases}
"""

def main():
    with open(settings.GRAMMAR_FILE) as g:
        # create parser
        parser = Lark(g.read(), start='equations')
        
        # create AST
        tree = parser.parse(equation)
        print(tree.pretty())

        # process AST using custom transformation
        print(LatexToEval().transform(tree).pretty())
    pass


if __name__ == "__main__":
    main()